`timescale 1ns/1ns

module ram_tb;
// Declare inputs as regs and outputs as wires
localparam WIDTH = 32;
localparam RAM_ADDR_BITS = 10;

reg[RAM_ADDR_BITS-1:0] addr;
reg[WIDTH-1:0]         data_in;
wire[WIDTH-1:0]        data_out;
reg                    wr_en, clk;

// Initialize all variables
initial begin        
    $dumpfile("ram_tb.lxt");
    $dumpvars(0,ram_tb);

    $display ("time\t addr         data_in                            data_out                           wr_en  clk");	
    $monitor ("%g\t %b   %b   %b     %b     %b", 
        $time, addr, data_in, data_out, wr_en, clk);	
    clk = 1;         // initial value of clock
    addr = 0;        // initial value of addr
    wr_en = 0;       // initial value of wr_en
    #6 addr =    10'h000;
    data_in = 32'h55555555;
    #1 wr_en = 1;    // Write the data.
    #10 wr_en = 0;    // Read a different address.
    #10 addr =   10'h001;
    data_in = 32'hcccccccc;
    #1 wr_en = 1;    // Write the data.
    #10 wr_en = 0;    // Read a different address.
    #10 addr =   10'h000;    // Read the first address again.
    #10 addr =   10'h001;    // Read the second address again.
    #5 $finish;      // Terminate simulation
end

// Clock generator
always begin
    #5 clk = ~clk; // Toggle clock every 5 ticks
end

// Connect RAM to test bench
ram ram1(addr, data_in, data_out, wr_en, clk);

endmodule

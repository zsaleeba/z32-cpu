module z32(reset, clk);

    parameter WIDTH = 32;
    parameter REG_ADDR_WIDTH = 5;
    parameter ALU_OP_WIDTH = 5;

    input reset;
    input clk;

    wire[WIDTH-1 : 0]          mem_addr;
    wire[WIDTH-1 : 0]          mem_dout;
    wire[WIDTH-1 : 0]          mem_din;
    wire                       mem_wren;

    wire[WIDTH-1 : 0]          src1_data, src2_data, dest_data;
    wire[WIDTH-1 : 0]          operand2;
    wire[WIDTH-1 : 0]          imm;
    wire[REG_ADDR_WIDTH-1 : 0] src1_addr, src2_addr, dest_addr;
    wire[ALU_OP_WIDTH-1 : 0]   alu_op;
    wire                       mem_addr_pc_src2_;
    wire[1:0]                  dest_data_from;
    wire                       dest_wr_en;
    wire                       mem_wr_en;
    wire                       inc_pc;
    wire                       pc_operand2_wr_en;

    wire[WIDTH-1 : 0]          pc_in, pc_out;
    wire[WIDTH-1 : 0]          alu_out;

    assign operand2 = src2_data + imm;
    assign mem_addr = mem_addr_pc_src2_ ? pc_out : operand2;

    assign dest_data = (dest_data_from == 2'b00) ? alu_out :
                    ( (dest_data_from == 2'b01) ? operand2 : 
                      ( (dest_data_from == 2'b10) ? mem_dout : 
                         pc_out
                      ) 
                    );

    // always @ *
    //     case (dest_data_from)
    //     2'b00: dest_data = alu_out;
    //     2'b01: dest_data = operand2;
    //     2'b10: dest_data = mem_dout;
    //     2'b11: dest_data = pc_out;
    //     endcase

    memory    mem1(mem_addr, mem_din, mem_dout, mem_wr_en, clk);
    iu        iu1(mem_dout, src1_data, operand2, imm, src1_addr, src2_addr, dest_addr, alu_op, mem_addr_pc_src2_, dest_data_from, dest_wr_en, mem_wr_en, inc_pc, pc_wr_en, reset, clk);
    registers regs1(src1_addr, src1_data, src2_addr, src2_data, dest_addr, dest_data, dest_wr_en, pc_out, inc_pc, operand2, pc_wr_en, reset, clk);
    alu       alu1(src1_data, operand2, alu_op, alu_out);

endmodule // z32

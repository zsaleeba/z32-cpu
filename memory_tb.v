`timescale 1ns/1ns

module memory_tb;
// Declare inputs as regs and outputs as wires
localparam WIDTH = 32;

reg[WIDTH-1:0]  addr;
reg[WIDTH-1:0]  data_in;
wire[WIDTH-1:0] data_out;
reg             wr_en, clk;

// Initialize all variables
initial begin        
    $dumpfile("memory_tb.lxt");
    $dumpvars(0, memory_tb);

    $display ("time\t addr         data_in                            data_out                           wr_en  clk");	
    $monitor ("%g\t %b   %b   %b     %b     %b", 
        $time, addr, data_in, data_out, wr_en, clk);	
    clk = 1;         // initial value of clock
    addr = 0;        // initial value of addr
    wr_en = 0;       // initial value of wr_en
    #6 addr =    32'h00000000;
    data_in = 32'h55555555;
    #1 wr_en = 1;    // Write the data.
    #10 wr_en = 0;    // Read a different address.
    #10 addr =   32'h00000004;
    data_in = 32'hcccccccc;
    #1 wr_en = 1;    // Write the data.
    #10 wr_en = 0;    // Read a different address.
    #10 addr =   32'h00000000;    // Read the first address again.
    #10 addr =   32'h00000004;    // Read the second address again.

    // Write a UART character.
    #5 addr = 32'h10000000;
    data_in = 32'h00000048;
    #5 wr_en = 1;    // Write the data.
    #5 wr_en = 0;

    data_in = 32'h00000065;
    #5 wr_en = 1;    // Write the data.
    #5 wr_en = 0;

    data_in = 32'h0000006c;
    #5 wr_en = 1;    // Write the data.
    #5 wr_en = 0;
    #5 wr_en = 1;    // Write the data.
    #5 wr_en = 0;

    data_in = 32'h0000006f;
    #5 wr_en = 1;    // Write the data.
    #5 wr_en = 0;

    #5 $finish;      // Terminate simulation
end

// Clock generator
always begin
    #5 clk = ~clk; // Toggle clock every 5 ticks
end

// Connect RAM to test bench
memory mem(addr, data_in, data_out, wr_en, clk);

endmodule

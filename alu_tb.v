`timescale 1ns/1ns

module alu_tb;
// Declare inputs as regs and outputs as wires
    parameter WIDTH = 32;

    reg[WIDTH-1 : 0]  x, y;
    reg[4 : 0]        op;
    wire[WIDTH-1 : 0] out;


// Initialize all variables
initial begin        
    $dumpfile("alu_tb.lxt");
    $dumpvars(0, alu_tb);

    // Initial values.
    x = 0;
    y = 0;
    op = 0;

    // All the ops.
    #10 x = 32'h1234;
    y = 32'h0c;
    
    #5 op = 5'h01;
    #5 op = 5'h02;
    #5 op = 5'h03;
    #5 op = 5'h04;
    #5 op = 5'h05;
    #5 op = 5'h06;
    #5 op = 5'h07;
    #5 op = 5'h08;
    #5 op = 5'h09;
    #5 op = 5'h0a;
    #5 op = 5'h0b;
    #5 op = 5'h0c;
    #5 op = 5'h0d;
    #5 op = 5'h0e;
    #5 op = 5'h0f;
    #5 op = 5'h10;
    #5 op = 5'h11;
    #5 op = 5'h12;
    #5 op = 5'h13;
    #5 op = 5'h14;
    #5 op = 5'h15;
    #5 op = 5'h16;
    #5 op = 5'h17;
    #5 op = 5'h18;
    #5 op = 5'h19;
    #5 op = 5'h1a;
    #5 op = 5'h1b;
    #5 op = 5'h1c;
    #5 op = 5'h1d;
    #5 op = 5'h1e;
    #5 op = 5'h1f;

    #20 $finish;      // Terminate simulation
end

// Connect ALU to test bench
alu alu1(x, y, op, out);

endmodule

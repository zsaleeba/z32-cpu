`timescale 1ns/1ns

module registers_tb;
// Declare inputs as regs and outputs as wires
    parameter WIDTH = 32;
    parameter REG_ADDR_WIDTH = 5;
    localparam REG_MAX = 1 << REG_ADDR_WIDTH;
    localparam PC_REG_ADDR = REG_MAX - 1;

    reg[REG_ADDR_WIDTH-1 : 0] port_a_addr, port_b_addr, port_c_addr;
    wire[WIDTH-1 : 0]         port_a_data, port_b_data;
    reg[WIDTH-1 : 0]          port_c_data;
    reg                       port_c_wr_en, inc_pc;
    wire[WIDTH-1 : 0]         pc_out;
    reg[WIDTH-1 : 0]          pc_in;
    reg                       pc_wr_en;
    reg                       reset;
    reg                       clk;


// Initialize all variables
initial begin        
    $dumpfile("registers_tb.lxt");
    $dumpvars(0, registers_tb);

    // Initial values.
    port_a_addr = 0;
    port_b_addr = 0;
    port_c_addr = 0;
    port_c_data = 32'h55555555;
    port_c_wr_en = 0;
    inc_pc = 0;
    pc_in = 0;
    pc_wr_en = 0;
    clk = 1;
    reset = 0;

    // Reset.
    #7 reset = 1;
    #5 reset = 0;

    // Write a value to register 1.
    #12 port_c_data = 32'h12345678;
    port_c_addr = 1;
    port_c_wr_en = 1;
    #10 port_c_wr_en = 0;

    // Write a value to register 2.
    port_c_data = 32'h9abcdef0;
    port_c_addr = 2;
    #10 port_c_wr_en = 1;
    #10 port_c_wr_en = 0;

    // Read from registers 0 and 1 on ports a and b.
    port_a_addr = 1;
    port_b_addr = 2;
    #20 port_a_addr = 3;
    port_b_addr = 4;
    #10 port_a_addr = 0;
    port_b_addr = 0;

    // Increment the PC a few times.
    #10 inc_pc = 1;
    #50 inc_pc = 0;

    // Write a value to the PC.
    pc_in = 32'hdeadbeef;
    pc_wr_en = 1;
    #10 pc_wr_en = 0;

    #20 $finish;      // Terminate simulation
end

// Clock generator
always begin
    #5 clk = ~clk; // Toggle clock every 5 ticks
end

// Connect RAM to test bench
registers registers1(port_a_addr, port_a_data, port_b_addr, port_b_data, port_c_addr, port_c_data, port_c_wr_en, pc_out, inc_pc, pc_in, pc_wr_en, reset, clk);

endmodule

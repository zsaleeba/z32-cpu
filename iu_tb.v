`timescale 1ns/1ns

module iu_tb;
// Declare inputs as regs and outputs as wires
    parameter WIDTH = 32;
    parameter REG_ADDR_WIDTH = 5;
    parameter ALU_OP_WIDTH = 5;
    parameter REG_ADDR_PC = 31;

    reg[WIDTH-1 : 0]           instr_data;
    reg[WIDTH-1 : 0]           operand1, operand2;
    wire[WIDTH-1 : 0]          imm;
    wire[REG_ADDR_WIDTH-1 : 0] src1_addr, src2_addr, dest_addr;
    wire[ALU_OP_WIDTH-1 : 0]   alu_op;
    wire                       ram_addr_pc_src2_;
    wire[1:0]                  dest_data_from;
    wire                       dest_wr_en;
    wire                       ram_wr_en;
    wire                       inc_pc;
    wire                       pc_operand2_wr_en;
    reg                        reset;
    reg                        clk;

// Initialize all variables
initial begin        
    $dumpfile("iu_tb.lxt");
    $dumpvars(0, iu_tb);

    // Initial state.
    clk = 1;
    reset = 0;
    instr_data = 0;
    operand1 = 0;
    operand2 = 0;

    // Reset.
    #7 reset = 1;
    #6 reset = 0;

    // Two register add.
    #13 instr_data = 32'h500886ab;
    #20 operand2 = 32'h12345678;

    // Prefix add.
    #20 instr_data = 32'h20654321;
    #20 operand2   = 32'h00654321;
    #20 instr_data = 32'h50088687;
    #20 operand2   = 32'h87654321;

    // movi r1,123456.
    #20 instr_data = 32'h21123456;
    #20 operand2   = 32'h00123456;

    // jmp = prefix mov.
    #20 instr_data = 32'h20345678;
    #20 operand2   = 32'h00345678;
    #20 instr_data = 32'h40003e12;
    #20 operand2   = 32'h12345678;

    // jal to $13243546. pc -> reg 1d
    #20 instr_data = 32'h20243546;
    #20 operand2   = 32'h00243546;
    #20 instr_data = 32'h01003b13;
    #20 operand2   = 32'h13243546;

    // nop.
    #20 instr_data = 32'h02000000;
    #20 operand2   = 32'h00000000;

    // bz $12 (taken).
    #20 instr_data = 32'h040f8012;
    #10 operand1   = 32'h00000000;
    #10 operand2   = 32'h0000ab12;
    
    // bz $34 (not taken).
    #20 instr_data = 32'h040f8034;
    #10 operand1   = 32'h00000001;
    #10 operand2   = 32'h0000ab34;
    
    // bnz $56 (not taken).
    #20 instr_data = 32'h050f8056;
    #10 operand1   = 32'h00000000;
    #10 operand2   = 32'h0000ab56;
    
    // bnz $ffffff78 (taken).
    #20 instr_data = 32'h050f8178;
    #10 operand1   = 32'h00000001;
    #10 operand2   = 32'hffffff78;

    // rd r2,pc+$12.
    #20 operand1   = 32'h00000000;
    instr_data     = 32'h10078012;
    #20 operand2   = 32'h00003412;

    // wr r3,pc+$34.
    #20 instr_data = 32'h181f8034;
    #20 operand2   = 32'h00003434;
    
    
    #30 $finish;      // Terminate simulation
end

// Clock generator
always begin
    #5 clk = ~clk; // Toggle clock every 5 ticks
end

// Connect RAM to test bench
iu iu1(instr_data, operand1, operand2, imm, src1_addr, src2_addr, dest_addr, alu_op, ram_addr_pc_src2_, dest_data_from, dest_wr_en, ram_wr_en, inc_pc, pc_operand2_wr_en, reset, clk);

endmodule

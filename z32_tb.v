`timescale 1ns/1ns

module z32_tb;
// Declare inputs as regs and outputs as wires
reg reset;
reg clk;

// Initialize all variables
initial begin        
    $dumpfile("z32_tb.lxt");
    $dumpvars(0, z32_tb);

    // Initial state.
    clk = 1;
    reset = 0;

    // Reset.
    #7 reset = 1;
    #6 reset = 0;    
    
    #3000 $finish;      // Terminate simulation
end

// Clock generator
always begin
    #5 clk = ~clk; // Toggle clock every 5 ticks
end

// Connect CPU to test bench
z32 z32_1(reset, clk);

endmodule

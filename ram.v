module ram(addr, data_in, data_out, wr_en, clk);

    parameter WIDTH = 32;
    parameter ADDR_BITS = 10;
    localparam ADDR_MAX = 1 << ADDR_BITS;

    input[ADDR_BITS-1 : 0]  addr;
    input[WIDTH-1 : 0]      data_in;
    input                   wr_en;
    output reg[WIDTH-1 : 0] data_out;
    input                   clk;

    // RAM cells.
    reg[WIDTH-1 : 0] mem [0 : ADDR_MAX-1]; // = '{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    // Initial RAM contents.
    initial begin
        $readmemh("program.hex", mem);
    end

    // RAM read.
    always @*
    begin
        data_out = mem[addr];
    end

    // RAM write.
    always @(posedge clk)
    begin
        if (wr_en)
            mem[addr] <= data_in;
    end
endmodule // ram

// Register file.
module registers(port_a_addr, port_a_data, port_b_addr, port_b_data, port_c_addr, port_c_data, port_c_wr_en, pc_out, inc_pc, pc_in, pc_wr_en, reset, clk);

    parameter WIDTH = 32;
    parameter REG_ADDR_WIDTH = 5;
    localparam REG_MAX = 1 << REG_ADDR_WIDTH;
    localparam PC_REG_ADDR = REG_MAX - 1;

    input[REG_ADDR_WIDTH-1 : 0] port_a_addr, port_b_addr, port_c_addr;
    output reg[WIDTH-1 : 0]     port_a_data, port_b_data;
    input[WIDTH-1 : 0]          port_c_data;
    input                       port_c_wr_en, inc_pc;
    output reg[WIDTH-1 : 0]     pc_out;
    input[WIDTH-1 : 0]          pc_in;
    input                       pc_wr_en;
    input                       reset;
    input                       clk;

    // Registers.
    reg[WIDTH-1 : 0] reg_store [REG_MAX : 0];

    // Register ports.

    // Port A is an asynchronous read only port.
    always @(port_a_addr)
    begin
        if (port_a_addr == 0)
            port_a_data <= 0;
        else
            port_a_data <= reg_store[port_a_addr];
    end

    // Port B is an asynchronous read only port.
    always @(port_b_addr)
    begin
        if (port_b_addr == 0)
            port_b_data <= 0;
        else
            port_b_data <= reg_store[port_b_addr];
    end

    // Port C is a synchronous write only port.
    // Writes only occur if port_c_wr_en is 1.
    always @(posedge clk)
    begin
        if (port_c_wr_en)
            reg_store[port_c_addr] <= port_c_data;
    end

    // PC handling.
    always @(posedge clk or reset)
    begin
        if (reset) begin
            reg_store[PC_REG_ADDR] <= 0;
        end else begin
            if (pc_wr_en)
                reg_store[PC_REG_ADDR] <= pc_in;
            else if (inc_pc)
                reg_store[PC_REG_ADDR] <= reg_store[PC_REG_ADDR] + 4;
        end
    end

    always @(reg_store[PC_REG_ADDR])
    begin
        pc_out <= reg_store[PC_REG_ADDR];
    end

endmodule // registers

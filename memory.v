//
// Implements the memory map of the system.
// 
// Memory map:
// $00000000 - $00003ff - RAM memory.
// $10000000            - UART output.
//

module memory(addr, data_in, data_out, wr_en, clk);

    parameter WIDTH = 32;
    parameter ADDR_BITS = 32;
    parameter RAM_ADDR_BITS = 10;
    parameter ADDRS_PER_WORD = 2;
    localparam RAM_MAX_BITS = RAM_ADDR_BITS + ADDRS_PER_WORD;

    input[WIDTH-1 : 0]  addr;
    input[WIDTH-1 : 0]  data_in;
    input               wr_en;
    output[WIDTH-1 : 0] data_out;
    input               clk;

    wire                ram_en;
    wire[WIDTH-1 : 0]   ram_data_out;

    // RAM memory.
    assign ram_en = addr[ADDR_BITS-1:RAM_MAX_BITS] == 20'h00000;

    ram ram1(addr[RAM_MAX_BITS-1:ADDRS_PER_WORD], data_in, ram_data_out, ram_en && wr_en, clk);

    assign data_out = ram_en ? ram_data_out : 0;

    // UART output.
    assign uart_out_en = addr == 32'h10000000;

    always @(posedge clk)
    begin
        if (uart_out_en && wr_en) begin
            $monitor("%g uart: %c\n", $time, data_in);
        end
    end


endmodule // memory

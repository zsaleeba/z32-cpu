MODULE = z32
SRCS = ram.v memory.v registers.v alu.v iu.v z32.v


all: $(MODULE).lxt


#
# Pattern rules.
#

%.lxt: %.vvp
	vvp -n $< -lxt

%.vvp: %.v
	iverilog -o $@ $^


#
# Main simulation.
#

$(MODULE).vvp: $(SRCS)

$(MODULE).lxt: $(MODULE).vvp

show: $(MODULE).lxt
	gtkwave $<


#
# Test benches.
#

ram_tb.vvp: ram.v ram_tb.v
ram_test: ram_tb.lxt
	gtkwave $<

registers_tb.vvp: registers.v registers_tb.v
registers_test: registers_tb.lxt
	gtkwave $<

alu_tb.vvp: alu.v alu_tb.v
alu_test: alu_tb.lxt
	gtkwave $<

memory_tb.vvp: ram.v memory.v memory_tb.v
memory_test: memory_tb.lxt
	gtkwave $<

iu_tb.vvp: iu.v iu_tb.v
iu_test: iu_tb.lxt
	gtkwave $<

z32_tb.vvp: ram.v memory.v registers.v alu.v iu.v z32.v z32_tb.v program.hex
	iverilog -o $@ ram.v memory.v registers.v alu.v iu.v z32.v z32_tb.v

z32_test: z32_tb.lxt
	gtkwave $<


#
# Maintenance.
#

clean:
	rm -f *.vvp *.vcp *.lxt

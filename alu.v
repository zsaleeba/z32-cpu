// Arithmetic logic unit.
module alu(x, y, op, out);

    parameter WIDTH = 32;

    input[WIDTH-1 : 0]      x, y;
    input[4 : 0]            op;
    output reg[WIDTH-1 : 0] out;

    always @(x or y or op)
    begin
        case (op)
            5'b00000: out <= x;
            5'b00001: out <= x | y;
            5'b00010: out <= x & y;
            5'b00011: out <= x ^ y;
            5'b00100: out <= x << y;
            5'b00101: out <= x >> y;
            5'b01000: out <= x < y;
            5'b01001: out <= x <= y;
            5'b01010: out <= x > y;
            5'b01011: out <= x >= y;
            5'b01100: out <= x != y;
            5'b01101: out <= x == y;
            5'b10000: out <= x + y;
            5'b10001: out <= x - y;
            5'b10100: out <= x * y;
            5'b10110: out <= x / y;
            5'b10111: out <= x % y;

            default:  out <= x;
        endcase
    end

endmodule // alu

// Instruction unit.
//
// Instruction execution has three phases:
//    1. Present address for next instruction read. 
//    2. Instruction decode, read registers.
//    3. ALU operation.
//    4. Write result to register or memory.
//
// Ports:
//    instr_data - instruction word received from the program memory.
//    operand1   - from reg[src1_addr].
//    operand2   - the result of adding reg[src2_addr] + imm.
//    imm        - the immediate value.
//    src1_addr  - the register address of the src1 register.
//    src2_addr  - the register address of the src2 register.
//    dest_addr  - the register address of the dest register.
//    alu_op     - the ALU operation to run.
//    ram_addr_pc_operand2_ - if 1 take the ram address from the pc, otherwise
//                 from src2 + imm.
//    dest_data_from - take the destination register data from:
//                     00: alu
//                     01: src2 + imm
//                     10: memory
//                     11: pc
//    dest_wr_en - if 1 write to a register.
//    ram_wr_en  - if 1 permit writing to the RAM.
//    inc_pc     - if 1 increment the PC.
//    pc_operand2_wr_en - if 1 write operand2 to the PC.
//

module iu(instr_data, operand1, operand2, imm, src1_addr, src2_addr, dest_addr, alu_op, ram_addr_pc_operand2_, dest_data_from, dest_wr_en, ram_wr_en, inc_pc, pc_operand2_wr_en, reset, clk);

    parameter WIDTH = 32;
    parameter REG_ADDR_WIDTH = 5;
    parameter ALU_OP_WIDTH = 5;
    parameter REG_ADDR_PC = 31;

    input[WIDTH-1 : 0]               instr_data;
    input[WIDTH-1 : 0]               operand1, operand2;
    output reg[WIDTH-1 : 0]          imm;
    output reg[REG_ADDR_WIDTH-1 : 0] src1_addr, src2_addr, dest_addr;
    output reg[ALU_OP_WIDTH-1 : 0]   alu_op;
    output reg                       ram_addr_pc_operand2_;
    output reg[1:0]                  dest_data_from;
    output reg                       dest_wr_en;
    output reg                       ram_wr_en;
    output reg                       inc_pc;
    output reg                       pc_operand2_wr_en;
    input                            reset;
    input                            clk;
    
    reg[1:0]                         instr_state;
    reg[7:0]                         opcode;
    reg[WIDTH-1 : 0]                 ir;

    reg                              dest_wr_en_next;
    reg                              ram_wr_en_next;
    reg                              pc_operand2_wr_en_next;
    reg                              instr_is_prefix;
    reg                              reset_stretched;

    localparam [1:0] // The four CPU instruction states.
        INSTR_FETCH  = 2'b00,
        INSTR_DECODE = 2'b01, 
        INSTR_ALU    = 2'b10,
        INSTR_WRITE  = 2'b11;

    parameter [1:0] // Three possible sources for data to the register file.
        DEST_DATA_ALU      = 2'b00,
        DEST_DATA_OPERAND2 = 2'b01,
        DEST_DATA_MEM      = 2'b10,
        DEST_DATA_PC       = 2'b11;

    // Initial state.
    initial begin
        // instr_state <= INSTR_FETCH;
        // dest_wr_en <= 0;
        // ram_wr_en <= 0;
        // instr_is_prefix <= 0;
    end

    // State machine progress.
    always @(posedge clk)
    begin
        if (reset_stretched) begin
            instr_state <= INSTR_FETCH;
            instr_is_prefix <= 0;

            imm <= 0;
            src1_addr <= 0;
            src2_addr <= 0;
            dest_addr <= 0;
            alu_op <= 0;
            ram_addr_pc_operand2_ <= 1;
            dest_data_from <= DEST_DATA_ALU;
            dest_wr_en_next <= 0;
            dest_wr_en <= 0;
            ram_wr_en_next <= 0;
            ram_wr_en <= 0;
            inc_pc <= 0;
            pc_operand2_wr_en <= 0;
            pc_operand2_wr_en_next <= 0;
            opcode <= 0;
            ir <= 0;
        end

        reset_stretched <= reset;
    end

    always @(negedge clk)
    begin
        if (!reset_stretched)
            instr_state <= instr_state + 1;

        dest_wr_en        <= dest_wr_en_next;
        ram_wr_en         <= ram_wr_en_next;
        pc_operand2_wr_en <= pc_operand2_wr_en_next;
    end

    always @(posedge reset)
    begin
        reset_stretched <= 1;
    end

    // Instruction fetch.
    always @(posedge clk)
    begin
        case (instr_state)
            INSTR_FETCH: begin
                // Fetches the instruction from memory.
                ram_addr_pc_operand2_ <= 1;  // RAM address is the PC.
            end

            INSTR_DECODE: begin
                // Decodes the instruction, retrieves registers and adds 
                // the immediate value.
                inc_pc <= 1;

                ir <= instr_data;
                opcode <= instr_data[31:24];
                alu_op <= instr_data[28:24];
                src1_addr <= instr_data[23:19];
                src2_addr <= instr_data[18:14];
                dest_addr <= instr_data[13:9];

                // Immediate values.
                if (instr_data[31:29] == 3'b001)
                begin
                    imm <= instr_data[23:0];                // Prefix instruction.
                end else begin
                    if (instr_is_prefix)
                    begin
                        imm <= {instr_data[8:0], imm[23:0]};    // Use the previous prefix.
                    end else begin
                        imm <= $signed(instr_data[8:0]);        // Just a short value.
                    end
                end
            end

            INSTR_ALU: begin
                // Performs the instruction operation.
                instr_is_prefix <= 0;
                dest_wr_en_next <= 0;
                inc_pc <= 0;
                dest_data_from <= DEST_DATA_ALU;

                case (opcode[7:5])
                    3'b000: begin
                        // swi / jal / nop / bxx.
                        case (opcode[4:0])
                            5'b00000: ; // swi.

                            5'b00001: begin // jal.
                                // reg[dest] <- PC.
                                dest_data_from <= DEST_DATA_PC;
                                dest_wr_en_next <= 1;

                                // PC <- operand2.
                                pc_operand2_wr_en_next <= 1;
                            end

                            5'b00100: begin // bz.
                                dest_data_from <= DEST_DATA_PC;

                                if (operand1 == 0)
                                    pc_operand2_wr_en_next <= 1;
                            end

                            5'b00101: begin // bnz.
                                dest_data_from <= DEST_DATA_PC;

                                if (operand1 != 0)
                                    pc_operand2_wr_en_next <= 1;
                            end

                            5'b10000: begin // rd.
                                ram_addr_pc_operand2_ <= 0;
                                dest_data_from <= DEST_DATA_MEM;
                                dest_wr_en_next <= 1;
                            end

                            5'b11000: begin // wr.
                                ram_addr_pc_operand2_ <= 0;
                                ram_wr_en_next <= 1;
                            end

                            default: ;  // nop.
                        endcase
                    end

                    3'b001: begin
                        // pfx / movi.
                        if (opcode[4:0] == 0) begin
                            // Use the immediate value out of imm next instruction.
                            instr_is_prefix <= 1;       
                        end else begin
                            // Store it in a register next cycle.
                            dest_wr_en_next <= 1;
                        end

                        // Change the destination register to reflect the movi.
                        dest_addr <= opcode[4:0];
                        dest_data_from <= DEST_DATA_OPERAND2;
                    end

                    3'b010: begin
                        // ALU ops. Data propagates through the ALU 
                        // asynchronously. Store the result next cycle.
                        dest_wr_en_next <= 1;
                    end
                endcase
            end

            INSTR_WRITE: begin
                // Writes back results to a register or memory location.

                // Finish any write pulses we were doing previously.
                dest_wr_en_next <= 0;
                ram_wr_en_next <= 0;
                inc_pc <= 0;
                pc_operand2_wr_en_next <= 0;
            end
        endcase
    end

endmodule // iu.
